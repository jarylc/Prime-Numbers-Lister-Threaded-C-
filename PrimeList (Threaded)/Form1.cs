﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Management;
using System.Diagnostics;

namespace PrimeList__Threaded_
{
    public partial class Form1 : Form
    {
        private ArrayList primes = new ArrayList();
        
        private int threads;
        private int finished;

        private Stopwatch calcWatch = new Stopwatch();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (txtThreads.Text.Trim() == string.Empty || txtMin.Text.Trim() == string.Empty || txtMax.Text.Trim() == string.Empty)
            {
                txtOutput.Text = "ERROR: Some fields are empty!";
                return;
            }

            int min = Convert.ToInt32(txtMin.Text);
            int max = Convert.ToInt32(txtMax.Text);
            if (min < 0 || max < 0)
            {
                txtOutput.Text = "ERROR: No negative values allowed!";
                return;
            }
            if (min >= max)
            {
                txtOutput.Text = "ERROR: Minimum value is more or equal to maximum!";
                return;
            }

            this.Enabled = false;
            txtOutput.Text = "";

            threads = Convert.ToInt32(txtThreads.Text); // total number of threads specified by user to use
            if (threads > max)
            {
                threads = max;
                txtThreads.Text = max.ToString();
            }

            // this is to find the exponential interval
            double interval = Math.Log((max - min), threads) / threads;

            int nMax = max; // store max in another temporary integer container
            for (int i = 0; i < threads; i++)
            {
                // formula to ensure that the threads processing larger values will have lesser values to process
                // in return threads processing smaller values will have more values to process
                // this exponential formula isn't perfect, but it helps speed it up slightly as compared to dividing equal parts
                // if thread count = 1 or it's the last iteration, nMin = specified minimum, else use formula below
                // formula: min = (max - (totalThreads ^ (exponentialInterval * (iteration + 1))))
                // after which, nMax = (above result) - 1 and go to next iteration if any
                int nMin = ((i + 1 == threads || threads == 1) ? min : max - (int)(Math.Floor((Math.Pow(threads, (interval * (i + 1)))))));

                PrimeThread primeThread = new PrimeThread(nMin, nMax);

                var bw = new BackgroundWorker();
                bw.DoWork += primeThread.calculate;
                bw.RunWorkerCompleted += checkDone;
                bw.RunWorkerAsync();

                nMax = nMin - 1;
            }
            calcWatch.Restart();
        }

        private void checkDone(object sender, RunWorkerCompletedEventArgs e)
        {
            primes.AddRange((ArrayList)e.Result);

            finished++;
            if (finished < threads)
            {
                return;
            }

            calcWatch.Stop();

            Stopwatch listWatch = new Stopwatch();
            listWatch.Start();

            if (chkSort.Checked)
                primes.Sort();
            /*foreach (PrimeThread primeThread in primeThreads)
                primes.AddRange(primeThread.primes); // seemed faster to combine first and print later, rather than print per thread*/
            txtOutput.AppendText(string.Join("\t", primes.ToArray()) + "\t");

            listWatch.Stop();

            txtOutput.AppendText("\r\n\r\nTotal primes: " + primes.Count);
            txtOutput.AppendText("\r\nTotal milliseconds to calculate: " + calcWatch.ElapsedMilliseconds);
            txtOutput.AppendText("\r\nTotal milliseconds to list: " + listWatch.ElapsedMilliseconds);
            txtOutput.AppendText("\r\nStopwatch: " + (Stopwatch.IsHighResolution ? "Optimized" : "Legacy") + " (affected by hardware)");

            finished = 0;
            primes.Clear();
            this.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // set high priorities for benchmarking
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
            Thread.CurrentThread.Priority = ThreadPriority.Highest;

            txtThreads.Text = getOptimumThreadsCount();
        }

        // cores * 2
        private string getOptimumThreadsCount()
        {
            foreach (var item in new System.Management.ManagementObjectSearcher("Select NumberOfCores from Win32_Processor").Get())
            {
                return (Convert.ToInt32(item["NumberOfCores"]) * 2).ToString();
            }
            return String.Empty;
        }
    }
}
