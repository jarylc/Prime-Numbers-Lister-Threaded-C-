﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PrimeList__Threaded_
{
    class PrimeThread
    {
        public ArrayList primes = new ArrayList();

        public int start { get; set; }
        public int end { get; set; }

        public PrimeThread() { }
        public PrimeThread(int start, int end)
        {
            this.start = start;
            this.end = end;
        }

        public void calculate(object sender, DoWorkEventArgs e)
        {
            for (int i = start; i <= end; i++)
            {
                check(i);
            }

            e.Result = primes;
        }

        private void check(int num)
        {
            if ((num != 2 && num % 2 == 0) || num == 1) // if not 2, not divisible by 2 and not 1
                return;

            for (int i = 3; i <= (Math.Sqrt(num)); i++)
            {
                if (num % i == 0) // from 3, if square root of number / iteration is not divisible to a whole number
                    return; // reject this number and continue iteration if any
            }

            if (!primes.Contains(num))
            {
                primes.Add(num); // add to the storage if it passes the prime check
            }
        }
    }
}
